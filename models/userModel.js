const mongoose= require("mongoose");

const userSchema  = new mongoose.Schema({

    name:{
        type:String,
        required:true
    },
    userid:{
        type:Number,
        requred:true
    },
    email:{
        type:String,
        required:true
    },
    mobilenumber:{
        type:Number,
        required:true
    },
    image:{
        type:String,
        required:true,
    },
    password:{
        type:String,
        required:true
    },
    usertype:{
        type:String,
        required:true
    },
    studentorstaff:{
        type:String,
        required:true
    },
    year:{
        type:Number,
        required:true

    },
    // section:{
    //     type:String,
    //     required:true

    // },
    department:{
        type:String,
        required:true,

    },
    is_verified:{
        type:Number,
        default:0
    },
    token:{
        type:String,
        default:''
    }

});

module.exports = mongoose.model("User",userSchema);