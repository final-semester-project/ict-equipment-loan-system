const Item = require('../models/item');
const Category = require('../models/Category');
const adminAuth = require('../middleware/adminAuth');
const auth = require('../middleware/auth')
const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const randomstring = require('randomstring');
const config = require('../config/config');
const nodemailer = require('nodemailer')


const viewItems = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const adminData = await User.findById({ _id: req.session.user_id });
      const searchQuery = req.query.search;
      let items = await Item.find().populate('category', 'name');

      if (searchQuery) {
        items = items.filter(item => item.name.includes(searchQuery) || item.category?.name?.includes(searchQuery));
      }

      res.render('item', { items, searchQuery, admin:adminData });
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};
const viewUserItems = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    auth.isLogin(req, res, async () => {
      const adminData = await User.findById({ _id: req.session.user_id });
      const searchQuery = req.query.search;
      let items = await Item.find().populate('category', 'name');

      if (searchQuery) {
        items = items.filter(item => item.name.includes(searchQuery) || item.category?.name?.includes(searchQuery));
      }

      res.render('viewitem', { items, searchQuery, admin:adminData });
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};
// View a itemdetails from user side
const viewItemDetails = async (req, res) => {
  try {
    const itemId = req.params.id;
    const item = await Item.findById(itemId);

    res.render('itemDetails', { item });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};
// View a itemdetails from user side
const AdminviewItemDetails = async (req, res) => {
  try {
    const adminData = await User.findById({ _id: req.session.user_id });
    const itemId = req.params.id;
    const item = await Item.findById(itemId).populate('category');
    console.log(item);

    res.render('itemdetails', {item, admin:adminData });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};



const getAddItem=async(req,res)=>{

  // Fetch categories
  const adminData = await User.findById({ _id: req.session.user_id });
  const categories = await Category.find();
  res.render('add-item', { categories:categories, admin:adminData}); // Pass categories to view
}
const addItem = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const name = req.body.name;
      const category = req.body.category;
      const description = req.body.description;
      const image = req.file.filename;
      const available_items = req.body.available_items;


      const newItem = new Item({ name, category, description, image, available_items });
      await newItem.save();      
      res.redirect('/admin/items');
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};
const getEditItem = async (req, res) => {
  // Fetch item id and categories from database
  const adminData = await User.findById({ _id: req.session.user_id });
  const item = await Item.findById(req.params.id).populate('category', 'name');
  const categories = await Category.find();
  console.log("Hellloooooo"+categories)
  res.render('edit-item', { categories:categories,item:item,admin:adminData }); // Pass categories to view
}
const editItemLoad = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const adminData = await User.findById({ _id: req.session.user_id });
      const item = await Item.findById(req.params.id).populate('category', 'name');
      res.render('edit-item', { item, admin:adminData});
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

const updateItem = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const { name, category, description, image, available_items } = req.body;
      await Item.findByIdAndUpdate(req.params.id, { name, category, description, image, available_items });
      res.redirect('/admin/items');
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

const deleteItemLoad = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const item = await Item.findById(req.params.id).populate('category', 'name');
      res.render('admin/item', { item });
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

const deleteItem = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      await Item.findByIdAndDelete(req.params.id);
      res.redirect('/admin/items');
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

module.exports = {
  viewItems,
  addItem,
  editItemLoad,
  updateItem,
  deleteItemLoad,
  deleteItem,
  getAddItem,
  getEditItem,
  viewUserItems,
  viewItemDetails,
  AdminviewItemDetails

};
