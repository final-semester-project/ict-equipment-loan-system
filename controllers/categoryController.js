const Category = require('../models/Category');
const adminAuth = require('../middleware/adminAuth');
const User = require('../models/userModel');


const viewCategories = async (req, res) => {
  try {
    const adminData = await User.findById({ _id: req.session.user_id });
    console.log("heloooooooo"+adminData)

    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const searchQuery = req.query.q || ''; // get search query parameter or default to empty string
      const categories = await Category.find({ name: { $regex: searchQuery, $options: 'i' } }); // filter categories based on search query
      res.render('category', {categories, searchQuery,admin:adminData });
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

const addCategory = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const { name } = req.body;
      const newCategory = new Category({ name });
      await newCategory.save();
      res.redirect('/admin/categories');
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};


const editCategoryLoad = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const category = await Category.findById(req.params.id);
      res.render('edit-category', { category});
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

const updateCategory = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const { name } = req.body;
      await Category.findByIdAndUpdate(req.params.id, { name });
      res.redirect('/admin/categories');
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

const deleteCategoryLoad = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      const category = await Category.findById(req.params.id);
      res.render('/admin/categories', { category });
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

const deleteCategory = async (req, res) => {
  try {
    // Use isLogin middleware to check if user is authenticated
    adminAuth.isLogin(req, res, async () => {
      await Category.findByIdAndDelete(req.params.id);
      res.redirect('/admin/categories');
    });
  } catch (error) {
    console.error(error);
    res.status(500).send('Server Error');
  }
};

module.exports = {
  viewCategories,
  addCategory,
  editCategoryLoad,
  updateCategory,
  deleteCategoryLoad,
  deleteCategory
};
