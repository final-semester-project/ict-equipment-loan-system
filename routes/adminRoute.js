const express=require("express");
const admin_route=express();

const session = require("express-session");
const config = require("../config/config");
admin_route.use(session({secret:config.sessionSecret, resave: false, // set to false to avoid deprecated warning
saveUninitialized: false}));
const Category=require('../models/Category')
const Item = require('../models/item')
const User = require('../models/userModel');

const bodyParser = require("body-parser");
admin_route.use(bodyParser.json());
admin_route.use(bodyParser.urlencoded({extended:true}));

admin_route.set('view engine','ejs');
admin_route.set('views', './views/admin');


const multer = require("multer");
const path = require("path");

admin_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});


const upload = multer({storage:storage});


const auth = require("../middleware/adminAuth");

const adminController = require("../controllers/adminController");
const categoryController = require('../controllers/categoryController');
const itemController = require('../controllers/itemController');


admin_route.get('/',auth.isLogout,adminController.loadLogin);


admin_route.post('/',adminController.verifyLogin);

admin_route.get('/adminhome',auth.isLogin,adminController.loadDashboard);

admin_route.get('/changepassword',adminController.LoadAdminPasswordChange);

admin_route.get('/viewalluser',auth.isLogin,adminController.LoadViewAllUser);


admin_route.get('/viewstudent',auth.isLogin,adminController.LoadViewstudent);


admin_route.get('/viewstaff',auth.isLogin,adminController.LoadViewstaff);

admin_route.get('/logout',auth.isLogin,adminController.logout);

admin_route.get('/forgetpassword',auth.isLogout,adminController.forgetLoad);
admin_route.post('/forgetpassword',adminController.forgetVerify);

admin_route.get('/forget-password',auth.isLogout,adminController.forgetPasswordLoad);
admin_route.post('/forget-password',adminController.resetPassword);

admin_route.get('/new-user',auth.isLogin,adminController.loadAddNewUser);

admin_route.post('/new-user',upload.single('image'),adminController.addUser);

admin_route.get('/adminprofile',adminController.LoadAdminProfile);
admin_route.get('/edit-adminprofile',adminController.editAdminProfileLoad);
admin_route.post('/edit-adminprofile',adminController.UpdateAdminProfile);
admin_route.get('/delete-admin',auth.isLogin,adminController.deleteUser);


// admin_route.post('/reset-password',adminController.passwordReset)

admin_route.get('/view-user',auth.isLogin,adminController.viewUserLoad);
admin_route.get('/view-onestaff',auth.isLogin,adminController.viewstaffUserLoad);

admin_route.get('/edit-user',auth.isLogin,adminController.editUserLoad);
admin_route.get('/edit-staff',auth.isLogin,adminController.editStaffLoad);

admin_route.post('/edit-user',adminController.updateUser);
admin_route.post('/edit-staff',adminController.updateStaff);

admin_route.get('/delete-user',auth.isLogin,adminController.deleteUserLoad);

admin_route.get('/delete-staff',adminController.deleteStaffUser);
admin_route.post('/delete-staff',adminController.confirmDeleteStaffUser);


admin_route.get('/categories', auth.isLogin, categoryController.viewCategories);

admin_route.get('/add-category', auth.isLogin, (req, res) => {
  res.render('categories');
});

admin_route.post('/add-category', auth.isLogin, categoryController.addCategory);


admin_route.get('/edit-category/:id', auth.isLogin, async (req, res) => {
  const category = await Category.findById(req.params.id);
  const adminData = await User.findById({ _id: req.session.user_id });
  res.render('edit-category', { category,admin:adminData });
});

admin_route.post('/edit-category/:id', auth.isLogin, categoryController.updateCategory);

admin_route.post('/categories/:id', auth.isLogin, categoryController.deleteCategory);


// Admin route to view all items
admin_route.get('/items', auth.isLogin, itemController.viewItems);
 
admin_route.get('/itemdetails/:id', auth.isLogin, itemController.AdminviewItemDetails);


// Admin route to load categories into the items_view
admin_route.get('/additem', auth.isLogin, itemController.getAddItem);
// Admin route to add a new item
admin_route.get('/additem', auth.isLogin, async (req, res) => {
  const adminData = await User.findById({ _id: req.session.user_id });
  const item = await Item.findById(req.params.id);
  res.render('additem', { item, admin:adminData });
});
admin_route.post('/additem',upload.single('image'), auth.isLogin, itemController.addItem);

admin_route.get('/edit-item/:id', auth.isLogin, itemController.getEditItem);

// Admin route to load the edit item form
admin_route.get('/edit-item/:id', auth.isLogin, async (req, res) => {
  const adminData = await User.findById({ _id: req.session.user_id });
  const item = await Item.findById(req.params.id);
  res.render('edit-item', { item, admin:adminData});
});
// Admin route to update an item
admin_route.post('/edit-item/:id', auth.isLogin, itemController.updateItem);
// Admin route to delete an item
admin_route.post('/item/:id',auth.isLogin, itemController.deleteItem);

admin_route.get('/itemDetails/:id', auth.isLogin, itemController.AdminviewItemDetails);


module.exports =admin_route;
