const User = require('../models/userModel');
const bcrypt = require('bcrypt');
const randomstring = require('randomstring');
const config = require('../config/config');
const nodemailer = require('nodemailer');

//secure password
const securePassword = async (password) => {
    try {
        const passwordHash = await bcrypt.hash(password, 10);
        return passwordHash;
    } catch (error) {

        console.log(error.message);

    }

}
//for reset password send mail
const sendResetPasswordMail = async (name, email, token) => {
    try {

        const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
                user: config.emailUser,
                pass: config.emailPassword
            }

        });
        const mailOptions = {
            from: config.emailUser,
            to: email,
            subject: 'Reset Password',
            html: '<p>Hii ' + name + ', please click here to <a href="http://127.0.0.1:3000/admin/forget-password?token=' + token + '">reset </a> your password.</p>'
        }
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            }
            else {
                console.log("Email has been sent:- ", info.response);
            }
        });

    } catch (error) {

        console.log(error.message);

    }
}
//to send mail for password and email
const addUserMail = async (name, email, password, user_id) => {
    try {

        const transporter = nodemailer.createTransport({
            host: 'smtp.gmail.com',
            port: 587,
            secure: false,
            requireTLS: true,
            auth: {
                user: config.emailUser,
                pass: config.emailPassword
            }

        });
        const mailOptions = {
            from: config.emailUser,
            to: email,
            subject: 'You has been added to ICT Equipment Loan System',
            html: '<p>Hii ' + name + ', you has been added to IELS <a href="http://127.0.0.1:3000/verify?id=' + user_id + '"></a></p> <br><br> <b>Email:-</b>' + email + '<br><b>Password:-</b>' + password + ''
        }
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                console.log(error);
            }
            else {
                console.log("Email has been sent:- ", info.response);
            }
        });

    } catch (error) {

        console.log(error.message);

    }
}


const loadLogin = async (req, res) => {
    try {

        res.render('adminlogin');

    } catch (error) {

        console.log(error.message);

    }
}

//verify admin login
const verifyLogin = async (req, res) => {
    try {

        const userid = req.body.userid;
        const password = req.body.password;

        const userData = await User.findOne({ userid: userid });
        if (userData) {

            const passwordMatch = await bcrypt.compare(password, userData.password);
            if (passwordMatch) {
                const usertype = userData.usertype;

                if (usertype === "admin") {
                    req.session.user_id = userData._id;
                    res.redirect("/admin/adminhome");


                }
                else if (usertype === "approval") {
                    req.session.user_id = userData._id;
                    res.redirect("/admin/approvalhome");
                }
                else {
                    req.session.user_id = userData._id;
                    res.redirect("/admin/userhome");
                }

            }
            else {

                res.render('adminlogin', { message: "User_ID and Password is incorrect" });
            }
        }
        else {
            res.render('adminlogin', { message: "User_ID and Password is incorrect" });
        }

    } catch (error) {

        console.log(error.message);

    }
}

//load admin dashboard
const loadDashboard = async (req, res) => {
    try {

        const userData = await User.findById({ _id: req.session.user_id });
        res.render('adminhome', { admin: userData });

    } catch (error) {

        console.log(error.message);

    }
}
//load view all user page
const LoadViewAllUser = async (req, res) => {
    try {
        

        const usersData = await User.find({ usertype: 3 });
        res.render('viewalluser', { users: usersData });

    } catch (error) {
        console.log(error.message);

    }
}
const LoadViewstudent = async (req, res) => {
    try {

        const adminData = await User.findById({ _id: req.session.user_id });

        const { search, year, department } = req.query;
        let query = { studentorstaff: "student" };
        if (search) {
            query.userid = search;
        }
        if (year) {
            query.year = year;
        }
        if (department) {
            query.department = department;
        }
        const usersData = await User.find(query);
        res.render("viewstudent", { users: usersData, admin:adminData, search, year, department });
    } catch (error) {
        console.log(error.message);
        res.status(500).send("server error");
    }
};

const LoadViewstaff = async (req, res) => {
    try {
        const adminData = await User.findById({ _id: req.session.user_id });

        const { search, department } = req.query;
        let query = { studentorstaff: "staff" };
        if (search) {
            query.userid = search;
        }
        if (department) {
            query.department = department;
        }
        const usersData = await User.find(query);
        res.render("viewstaff", { users: usersData, admin:adminData, search, department });
    } catch (error) {
        console.log(error.message);
        res.status(500).send("server error");
    }
};




//forget password
const forgetLoad = async (req, res) => {
    try {

        res.render('forgetpassword');

    } catch (error) {

        console.log(error.message);

    }
}

//verify email for forget password
const forgetVerify = async (req, res) => {
    try {

        const email = req.body.email;
        const userData = await User.findOne({ email: email });
        if (userData) {
            if (userData.is_admin == 0) {
                res.render('forgetpassword', { message: "Email is incorrect" });
            }
            else {
                const randomString = randomstring.generate();
                const updatedData = await User.updateOne({ email: email }, { $set: { token: randomString } });
                sendResetPasswordMail(userData.name, userData.email, randomString);
                res.render('forgetpassword', { message: "Please check your mail to reset your password." });
            }

        }
        else {
            res.render('forgetpassword', { message: "Email is incorrect" });
        }

    } catch (error) {

        console.log(error.message);

    }
}

//forget password page
const forgetPasswordLoad = async (req, res) => {
    try {

        const token = req.query.token;

        const tokenData = await User.findOne({ token: token });
        if (tokenData) {
            res.render('forget-password', { user_id: tokenData._id });

        }
        else {
            res.render('404', { message: "Invalid Link" });
        }

    } catch (error) {

        console.log(error.message);

    }
}

//reset password
const resetPassword = async (req, res) => {
    try {

        const password = req.body.password;
        const user_id = req.body.user_id;
        const securePass = await securePassword(password);
        const updatedData = await User.findByIdAndUpdate({ _id: user_id }, { $set: { password: securePass, token: '' } });
        res.redirect('/admin');
    } catch (error) {

        console.log(error.message);

    }
}
//new user add page load
const loadAddNewUser = async (req, res) => {
    try {
        const userData = await User.findById({ _id: req.session.user_id });
        res.render('addnewuser', { admin: userData });
        res.render('addnewuser');

    } catch (error) {

        console.log(error.message);

    }
}
//add user
const addUser = async (req, res) => {
    try {
        const adminData = await User.findById({ _id: req.session.user_id });
        console.log(req);
        const name = req.body.name;
        const userid = req.body.userid;
        const email = req.body.email;
        const mobilenumber = req.body.mno;
        const usertype = req.body.usertype;
        const year = req.body.year;
        // const section = req.body.department;
        const department = req.body.department;
        const studentorstaff = req.body.studentorstaff;
        const image = req.file.filename;
        const password = randomstring.generate(8);

        const finduser = await User.findOne({ email: email });
        const finduser1 = await User.findOne({ userid: userid });
        const finduser2 = await User.findOne({ mobilenumber: mobilenumber });
        if (finduser) {
            
            res.render('addnewuser', { message: "Email has already taken.", admin:adminData });
        }
        else if (finduser1) {
            res.render('addnewuser', { message: "User_ID has already taken.", admin:adminData });
        }
        else if (finduser2) {
            res.render('addnewuser', { message: "Mobile Number has already taken.", admin:adminData });
        }
        else {
            const spassword = await securePassword(password);
            const user = new User({
                name: name,
                userid: userid,
                email: email,
                mobilenumber: mobilenumber,
                usertype: usertype,
                studentorstaff: studentorstaff,
                year: year,
                // section: section,
                department: department,
                image: image,
                password: spassword,

            });
            const userData = await user.save();

            if (userData) {
                addUserMail(name, email, password, userData._id);
                res.render('addnewuser',{message:"User has been successfully added",admin:adminData});

            }

            else {

                res.render('addnewuser', { message: "Something wrong",admin:adminData })
            }

        }


    } catch (error) {

        console.log(error.message);

    }
}




//load admin profile load
const LoadAdminProfile = async (req, res) => {
    try {

        const userData = await User.findById({ _id: req.session.user_id });
        res.render('adminprofile', { admin: userData });

    } catch (error) {

        console.log(error.message);

    }
}

//admin edit adminprofile load
const editAdminProfileLoad = async (req, res) => {
    try {


        const id = req.query.id;
        const adminData = await User.findById({ _id: id });
        if (adminData) {

            res.render('editadminprofile', { admin: adminData });

        }
        else {

            res.redirect('/admin/adminhome');

        }
        res.render('editadminprofile');

    } catch (error) {
        console.log(error.message);

    }
}

//update edidted adminprofile
const UpdateAdminProfile = async (req, res) => {
    try {

        const userData = await User.findByIdAndUpdate({ _id: req.body.id },
            {
                $set: {
                    name: req.body.name,
                    userid: req.body.userid,
                    email: req.body.email,
                    mobilenumber: req.body.mno,
                }
            });

        res.redirect('/admin/adminhome');

    } catch (error) {
        console.log(error.message)

    }
}
//delete user
const deleteUser = async (req, res) => {
    try {

        const id = req.query.id;
        await User.deleteOne({ _id: id });
        res.redirect('/login');
    } catch (error) {

        console.log(error.message);

    }
}

//load admin password change
const LoadAdminPasswordChange = async (req, res) => {
    try {

        res.render('changepassword');

    } catch (error) {

        console.log(error.message);

    }
}
//edit password
// const passwordReset = async(req,res,next)=>{
//     const { oldPassword, newPassword, confirmPassword } = req.body;
//     const { userid } = req.session;
//     try {
//         const user = await User.findOne({ userid });
//         const passwordMatch = await bcrypt.compare(oldPassword, user.password);

//         if (!passwordMatch) {
//             return res.status(401).send('Old password does not match.');
//         }

//         if (newPassword !== confirmPassword) {
//           return res.status(400).send('New passwords do not match.');
//         }

//         const saltRounds = 10;
//         const hashedPassword = await bcrypt.hash(newPassword, saltRounds);
//         user.password = hashedPassword;

//         await user.save();
//         return res.send('Password reset successful.');


//     } catch (error) {
//         console.log(error.message);

//     }
// }
//admin logout
const logout = async (req, res) => {
    try {

        req.session.destroy();
        res.redirect('/login');

    } catch (error) {

        console.log(error.message);

    }
}

//admin user load
const viewUserLoad = async (req, res) => {
    try {
        const adminData = await User.findById({ _id: req.session.user_id });
        const id = req.query.id;
        const userData = await User.findById({ _id: id });
        if (userData) {

            res.render('viewuser', { users: userData, admin:adminData });

        }
        else {

            res.redirect('/admin/adminhome');

        }
        res.render('viewuser');

    } catch (error) {
        console.log(error.message);

    }
}
//admin user one staff load
const viewstaffUserLoad = async (req, res) => {
    try {
        const adminData = await User.findById({ _id: req.session.user_id });
        const id = req.query.id;
        const userData = await User.findById({ _id: id });
        if (userData) {

            res.render('viewonestaff', { users: userData, admin:adminData });

        }
        else {

            res.redirect('/admin/viewstaff');

        }
        res.render('viewonestaff');

    } catch (error) {
        console.log(error.message);

    }
}
//admin edit user load
const editUserLoad = async (req, res) => {
    try {

        const adminData = await User.findById({ _id: req.session.user_id });
        const id = req.query.id;
        const userData = await User.findById({ _id: id });
        if (userData) {

            res.render('edituser', { users: userData, admin:adminData });

        }
        else {

            res.redirect('/admin/viewstudent');

        }
        res.render('edituser');

    } catch (error) {
        console.log(error.message);

    }
}
//update edidted
const updateUser = async (req, res) => {
    try {

        const userData = await User.findByIdAndUpdate({ _id: req.body.id },
            {
                $set: {
                    name: req.body.name,
                    userid: req.body.userid,
                    email: req.body.email,
                    mobilenumber: req.body.mno,
                    year: req.body.year,
                    department: req.body.department
                }
            });

        res.redirect('/admin/viewstudent');

    } catch (error) {
        console.log(error.message)

    }
}

//admin edit staffuser load
const editStaffLoad = async (req, res) => {
    try {
        const adminData = await User.findById({ _id: req.session.user_id });
        const id = req.query.id;
        const userData = await User.findById({ _id: id });
        if (userData) {

            res.render('editstaff', { users: userData, admin:adminData });

        }
        else {

            res.redirect('/admin/viewstaff');

        }
        res.render('editstaff');

    } catch (error) {
        console.log(error.message);

    }
}

//update edidted staff
const updateStaff = async (req, res) => {
    try {

        const userData = await User.findByIdAndUpdate({ _id: req.body.id },
            {
                $set: {
                    name: req.body.name,
                    userid: req.body.userid,
                    email: req.body.email,
                    mobilenumber: req.body.mno,
                    usertype: req.body.usertype,
                    department: req.body.department
                }
            });

        res.redirect('/admin/viewstaff');

    } catch (error) {
        console.log(error.message)

    }
}


//delete user
const deleteUserLoad = async (req, res) => {
    try {

        const id = req.query.id;
        await User.deleteOne({ _id: id });
        res.redirect('/admin/viewalluser');
    } catch (error) {

        console.log(error.message);

    }
}

// // Controller function to get all pending loans
// const getAllPendingLoans = async(req, res) => {
//     try {
//       const loans = await Loan.find({ status: 'pending' }).populate('user_id item');
//       res.render('pending-loans', { loans });
//     } catch (error) {
//       next(error);
//     }
//   }

//   // Controller function to get all rejected loans
//   const getAllRejectedLoans = async(req, res) => {
//     try {
//       const loans = await Loan.find({ status: 'rejected' }).populate('user_id item');
//       res.render('rejected-loans', { loans });
//     } catch (error) {
//       next(error);
//     }
//   }

//   // Controller function to get all accepted loans
//   const getAllAcceptedLoans = async(req, res) => {
//     try {
//       const loans = await Loan.find({ status: 'approved' }).populate('user_id item');
//       res.render('accepted-loans', { loans });
//     } catch (error) {
//       next(error);
//     }
//   }
//   const updateLoanCollectionDate = async(req, res) => {
//     try {
//       const { loanId, collectionDate } = req.body;
//       const loan = await Loan.findById(loanId).populate('user_id item');
//       if (!loan) {
//         return res.status(404).send('Loan not found');
//       }
//       loan.admin_collection_date = collectionDate;
//       loan.status = 'approved';
//       await loan.save();

//       // Send email to the user with the collection date information
//       const user = await User.findById(loan.user_id);
//       const item = await Item.findById(loan.item);
//       const transporter = nodemailer.createTransport({
//         service: 'gmail',
//         auth: {
//           user: process.env.EMAIL_USER,
//           pass: process.env.EMAIL_PASS,
//         },
//       });
//       const mailOptions = {
//         from: process.env.EMAIL_USER,
//         to: user.email,
//         subject: 'Loan Approval and Collection Date Update',
//         text: `Dear ${user.name},\n\nYour loan request for ${loan.quantity} ${item.name}(s) has been approved.\nThe collection date is set to ${collectionDate}. Please make sure to collect your item(s) on or before this date.\n\nThank you,\nThe Admin`,
//       };
//       await transporter.sendMail(mailOptions);
//       res.redirect('/admin/pending-loans');
//     } catch (error) {
//       next(error);
//     }
//   }


  
  const deleteStaffUser = async (req, res) => {
    try {
      const id = req.query.id;
      const userData = await User.findById({ _id: id });
      if (userData) {
        res.render('deleteStaffUser', { users: userData });
      } else {
        res.redirect('/admin/viewstaff');
      }
    } catch (error) {
      console.log(error.message);
    }
  };
  
  const confirmDeleteStaffUser = async (req, res) => {
    try {
      const id = req.body.id;
      await User.deleteOne({ _id: id });
      res.redirect('/admin/viewstaff');
    } catch (error) {
      console.log(error.message);
    }
  };


module.exports = {
    loadLogin,
    verifyLogin,
    loadDashboard,
    LoadAdminProfile,
    editAdminProfileLoad,
    UpdateAdminProfile,
    // passwordReset,
    deleteUser,
    LoadAdminPasswordChange,
    loadAddNewUser,
    LoadViewstudent,
    LoadViewstaff,
    viewstaffUserLoad,
    logout,
    forgetLoad,
    forgetVerify,
    forgetPasswordLoad,
    resetPassword,
    addUser,
    viewUserLoad,
    editUserLoad,
    editStaffLoad,
    updateUser,
    updateStaff,
    deleteUserLoad,
    LoadViewAllUser,
    deleteStaffUser,
    confirmDeleteStaffUser
}
