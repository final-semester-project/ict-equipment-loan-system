const mongoose = require("mongoose");

mongoose.connect("mongodb://127.0.0.1:27017/usermanagementsystem");

const express = require("express");
const app = express();

const path = require("path");
app.use(express.static(path.join(__dirname, "static")));

//for user routes
const userRoute=require('./routes/userRoute');
// const user_route = require("./routes/userRoute");
app.use('/',userRoute);

//for admin routes
const adminRoute=require('./routes/adminRoute');
app.use('/admin',adminRoute);


app.listen(3000, function(){
    console.log("server is running...");
});