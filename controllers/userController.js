const User = require('../models/userModel');
const bcrypt= require('bcrypt');
const nodemailer = require('nodemailer');
const randormstring = require('randomstring');
const Item = require('../models/item');


const config = require ("../config/config");

const securePassword = async(password)=>{
    try {
        const passwordHash = await bcrypt.hash(password, 10);
        return passwordHash;
    } catch (error) {

        console.log(error.message);
        
    }

}
//to send mail for verification
const sendVerifyMail = async(name, email, user_id)=>{
    try {
        
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailPassword
            }

        });
        const mailOptions = {
            from:config.emailUser,
            to:email,
            subject:'Verification mail',
            html:'<p>Hii '+name+', please click here to <a href="http://127.0.0.1:3000/verify?id='+user_id+'">Verify</a> your mail.</p>'
        }
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:- ",info.response);
            }
        });

    } catch (error) {

        console.log(error.message);
        
    }
}

//for reset password send mail
const sendResetPasswordMail = async(name, email, token)=>{
    try {
        
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailPassword
            }

        });
        const mailOptions = {
            from:config.emailUser,
            to:email,
            subject:'Reset Password',
            html:'<p>Hii '+name+', please click here to <a href="http://127.0.0.1:3000/forget-password?token='+token+'">reset </a> your password.</p>'
        }
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:- ",info.response);
            }
        });

    } catch (error) {

        console.log(error.message);
        
    }
}

const loadRegister = async(req,res)=>{
    try{

        res.render('registration');

    }catch(error){
        console.log(error.message);
    }
}

const insertUser = async(req,res)=>{
    try {
            const spassword = await securePassword(req.body.password);
            const user = new User({
                name:req.body.name,
                userid:req.body.userid,
                email:req.body.email,
                mobilenumber:req.body.mno,
                usertype: req.body.usertype,
                year : req.body.year,
                // section : req.body.section,
                department:req.body.department,
                studentorstaff : req.body.studentorstaff,
                password:spassword,
                image:req.file.filename,
            });
            const finduser = await User.findOne({email:req.body.email});
            const finduser1 = await User.findOne({userid:req.body.userid});
            if(finduser){
                res.render('registration',{message:"Email already taken."});
            }
            else if(finduser1){
                res.render('registration',{message:"User_ID already taken."});
            }
            else{
                const userData = await user.save();
            
                if(userData){
                    //verify
                    sendVerifyMail(req.body.name, req.body.email, userData._id);
                    res.render('registration',{message:"Your registration has been successful, Please verify your email!"});

                }
                else{
                    res.render('registration',{message:"Your registration has been failed "});
                }
            }
        

    } catch (error) {

        console.log(error.message)
        
    }
}

const verifyMail = async(req,res)=>{
    try {

        const updateInfo = await User.updateOne({_id:req.query.id},{ $set:{ is_verified:1}});
        
        console.log(updateInfo);
        res.render("email-verified");


    } catch (error) {

        console.log(error.message);
        
    }
}
//login user method
const loginLoad = async(req, res )=>{
    try {
        
        res.render('login');

    } catch (error) {

        console.log(error.message)
        
    }
}
//verify login

const verifyLogin = async(req, res)=>{
    try {

        const userid = req.body.userid;
        const password = req.body.password;

        const userData = await User.findOne({userid:userid});

        if(userData){
            const passwordMatch = await bcrypt.compare(password,userData.password);
            if(passwordMatch){
                // if(userData.is_verified === 0){
                //     res.render('login', {message:"please verify your email."});
                // }
                if(userData.usertype === "admin"){
                    req.session.user_id = userData._id;                    
                    res.redirect('/admin/adminhome');   
                }
                // else if(userData.usertype === "approval"){
                //     req.session.user_id = userData._id;                    
                //     res.redirect('/approvalhome');   
                // }
                else{
                    req.session.user_id = userData._id;                    
                    res.redirect('/userhome');
                }
            }
            else{
                res.render('login',{message:"User_id or Password is incorrect!"});
            }
        }
        else{
            res.render('login',{message:"User_id or Password is incorrect!"});
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

const loadHome = async(req, res)=>{
    try {
        
        res.render('userhome');
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//user logout
const userLogout = async(req, res)=>{
    try {

        req.session.destroy();
        res.redirect('/');

        
    } catch (error) {

        console.log(error.message);
        
    }
}

//forget password 
const forgetLoad = async(req, res)=>{
    try {

        res.render('forget');
        
    } catch (error) {

        console.log(error.message);
        
    }
}

const forgetVerify = async(req, res)=>{
    try {

        const email = req.body.email;
        const userData = await User.findOne({email:email});
        if(userData){

            //email verifiied or not
            if(userData.is_verified ===0){
                res.render('forget',{message:"Please verify your email"});
            }
            else{
                const randomString = randormstring.generate();
                const updatedData = await User.updateOne({email:email},{$set:{token:randomString}});
                sendResetPasswordMail(userData.name, userData.email, randomString);
                res.render('forget', {message:"Please check your email to reset your password."});


            }
        }
        else{
            res.render('forget', {message:"User email is incorrect"});
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//load forget passward page
const forgetPasswordLoad = async(req,res)=>{
    try {

        const token = req.query.token;
        const tokenData  = await User.findOne({token:token});
        if(tokenData){
            res.render('forget-password',{user_id:tokenData._id});

        }
        else{
            res.render('404',{message:"token is invalid"});
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//reset password post 
const resetPassword = async(req, res)=>{
    try {

       const password = req.body.password;
       const user_id = req.body.user_id;
       const secure_password = await securePassword(password);

       const updatedData = await User.findByIdAndUpdate({_id:user_id},{$set:{password:secure_password, token:''}});

       res.redirect("/");

        
    } catch (error) {

        console.log(error.message);
        
    }
}

//user profile load
const userProfileLoad = async(req, res)=>{
    try {
        const userData = await User.findById({ _id:req.session.user_id});
        res.render('userprofile',{user:userData});
        
    } catch (error) {
        console.log(error.message);
        
    }
}

//for verification send mail link
// const verificationLoad = async(req, res)=>{
//     try {

//         res.render('verification');
        
//     } catch (error) {
//         console.log(error.message);
        
//     }
// }

//verification
// const sentVerificationLink = async(req, res)=>{
//     try {

//         const email = req.body.email;
//         const userData = await User.findOne({email:email});
//         if(userData){
//             sendVerifyMail(userData.fname, userData.lname, userData.email,userData._id);
//             res.render('verification',{message:"Reset verification mail sent to your mail Id, please check."});
//         }
//         else{

//             res.render('verification',{message:"This email does not exist"});


//         }
        
//     } catch (error) {

//         console.log(error.message);
        
//     }
// }

//user profile edit and load
const editUserProfileLoad = async(req, res)=>{
    try {

        const id = req.query.id;
        const userData = await User.findById({_id:id});
        if(userData){
            res.render('edituserprofile',{user:userData});
        }
        else{
            res.redirect('/userprofile');
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//update user profile
const UpdateUserProfile = async(req,res)=>{
    try {

        if(req.file){
            const userData = await User.findByIdAndUpdate({_id:req.body.user_id},{$set:{name:req.body.name,userid:req.body.userid,email:req.body.email,mobilenumber:req.body.mno,image:req.file.filename}});
        }
        else{

            const userData = await User.findByIdAndUpdate({_id:req.body.user_id},{$set:{name:req.body.name,userid:req.body.userid,email:req.body.email,mobilenumber:req.body.mno}});
        }
        res.redirect('/userprofile')
        
    } catch (error) {

        console.log(error.message);
        
    }
}

// const requestLoan = async (req, res) => {
//   try {
//     const item = await Item.findById(req.body.item_id);
//     if (!item) {
//       return res.status(400).json({ message: "Item not found" });
//     }
//     if (item.available_items < req.body.quantity) {
//       return res.status(400).json({ message: `Only ${item.available_items} items are available for this loan` });
//     }
//     const loan = new Loan({
//       loan_id: Math.floor(Math.random() * 1000000) + 1,
//       user_id: req.user._id,
//       item: item._id,
//       quantity: req.body.quantity,
//       return_date: req.body.return_date,
//     });
//     await loan.save();
//     res.status(201).json({ message: "Loan request submitted successfully", loan });
//   } catch (error) {
//     console.log(error.message);
//     res.status(500).json({ message: "Server error" });
//   }
// };
// const viewLoans = async (req, res) => {
//     try {
//       const loans = await Loan.find({ user_id: req.user._id });
//       res.render('loans', { loans });
//     } catch (error) {
//       console.log(error.message);
//       res.status(500).json({ message: "Server error" });
//     }
//   };
  



  

module.exports={
    loadRegister,
    insertUser,
    verifyMail,
    loginLoad,
    verifyLogin,
    loadHome,
    userLogout,
    forgetLoad,
    forgetVerify,
    forgetPasswordLoad,
    resetPassword,
    // verificationLoad,
    // sentVerificationLink
    userProfileLoad,
    editUserProfileLoad,
    UpdateUserProfile
   
}