const User = require('../models/userModel');
const Approval = require("../models/approval");
const Loan = require('../models/loan');
const bcrypt= require('bcrypt');
const nodemailer = require('nodemailer');
const randormstring = require('randomstring');
const Item = require('../models/item');


const config = require ("../config/config");

const securePassword = async(password)=>{
    try {
        const passwordHash = await bcrypt.hash(password, 10);
        return passwordHash;
    } catch (error) {

        console.log(error.message);
        
    }

}
//to send mail for verification
const sendVerifyMail = async(name, email, user_id)=>{
    try {
        
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailPassword
            }

        });
        const mailOptions = {
            from:config.emailUser,
            to:email,
            subject:'Verification mail',
            html:'<p>Hii '+name+', please click here to <a href="http://127.0.0.1:3000/verify?id='+user_id+'">Verify</a> your mail.</p>'
        }
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:- ",info.response);
            }
        });

    } catch (error) {

        console.log(error.message);
        
    }
}

//for reset password send mail
const sendResetPasswordMail = async(name, email, token)=>{
    try {
        
        const transporter = nodemailer.createTransport({
            host:'smtp.gmail.com',
            port:587,
            secure:false,
            requireTLS:true,
            auth:{
                user:config.emailUser,
                pass:config.emailPassword
            }

        });
        const mailOptions = {
            from:config.emailUser,
            to:email,
            subject:'Reset Password',
            html:'<p>Hii '+name+', please click here to <a href="http://127.0.0.1:3000/forget-password?token='+token+'">reset </a> your password.</p>'
        }
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
            }
            else{
                console.log("Email has been sent:- ",info.response);
            }
        });

    } catch (error) {

        console.log(error.message);
        
    }
}

const loadRegister = async(req,res)=>{
    try{

        res.render('registration');

    }catch(error){
        console.log(error.message);
    }
}

const insertUser = async(req,res)=>{
    try {
            const spassword = await securePassword(req.body.password);
            const user = new User({
                name:req.body.name,
                userid:req.body.userid,
                email:req.body.email,
                mobilenumber:req.body.mno,
                usertype: req.body.usertype,
                year : req.body.year,
                section : req.body.section,
                department:req.body.department,
                studentorstaff : req.body.studentorstaff,
                password:spassword,
                image:req.file.filename,
            });
            const finduser = await User.findOne({email:req.body.email});
            const finduser1 = await User.findOne({userid:req.body.userid});
            if(finduser){
                res.render('registration',{message:"Email already taken."});
            }
            else if(finduser1){
                res.render('registration',{message:"User_ID already taken."});
            }
            else{
                const userData = await user.save();
            
                if(userData){
                    //verify
                    sendVerifyMail(req.body.name, req.body.email, userData._id);
                    res.render('registration',{message:"Your registration has been successful, Please verify your email!"});

                }
                else{
                    res.render('registration',{message:"Your registration has been failed "});
                }
            }
        

    } catch (error) {

        console.log(error.message)
        
    }
}

const verifyMail = async(req,res)=>{
    try {

        const updateInfo = await User.updateOne({_id:req.query.id},{ $set:{ is_verified:1}});
        
        console.log(updateInfo);
        res.render("email-verified");


    } catch (error) {

        console.log(error.message);
        
    }
}
//login user method
const loginLoad = async(req, res )=>{
    try {
        
        res.render('login');

    } catch (error) {

        console.log(error.message)
        
    }
}
//verify login

const verifyLogin = async(req, res)=>{
    try {

        const userid = req.body.userid;
        const password = req.body.password;

        const userData = await User.findOne({userid:userid});

        if(userData){
            const passwordMatch = await bcrypt.compare(password,userData.password);
            if(passwordMatch){
                if(userData.is_verified === 0){
                    res.render('login', {message:"please verify your email."});
                }
                else if(userData.usertype === 1){
                    req.session.user_id = userData._id;                    
                    res.redirect('/admin/adminhome');   
                }
                else if(userData.usertype === 2){
                    req.session.user_id = userData._id;                    
                    res.redirect('/approvalhome');   
                }
                else{
                    req.session.user_id = userData._id;                    
                    res.redirect('/userhome');
                }
            }
            else{
                res.render('login',{message:"User_id or Password is incorrect!"});
            }
        }
        else{
            res.render('login',{message:"User_id or Password is incorrect!"});
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

const loadHome = async(req, res)=>{
    try {
        
        res.render('approvalhome');
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//user logout
const userLogout = async(req, res)=>{
    try {

        req.session.destroy();
        res.redirect('/');

        
    } catch (error) {

        console.log(error.message);
        
    }
}

//forget password 
const forgetLoad = async(req, res)=>{
    try {

        res.render('forget');
        
    } catch (error) {

        console.log(error.message);
        
    }
}

const forgetVerify = async(req, res)=>{
    try {

        const email = req.body.email;
        const userData = await User.findOne({email:email});
        if(userData){

            //email verifiied or not
            if(userData.is_verified ===0){
                res.render('forget',{message:"Please verify your email"});
            }
            else{
                const randomString = randormstring.generate();
                const updatedData = await User.updateOne({email:email},{$set:{token:randomString}});
                sendResetPasswordMail(userData.name, userData.email, randomString);
                res.render('forget', {message:"Please check your email to reset your password."});


            }
        }
        else{
            res.render('forget', {message:"User email is incorrect"});
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//load forget passward page
const forgetPasswordLoad = async(req,res)=>{
    try {

        const token = req.query.token;
        const tokenData  = await User.findOne({token:token});
        if(tokenData){
            res.render('forget-password',{user_id:tokenData._id});

        }
        else{
            res.render('404',{message:"token is invalid"});
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//reset password post 
const resetPassword = async(req, res)=>{
    try {

       const password = req.body.password;
       const user_id = req.body.user_id;
       const secure_password = await securePassword(password);

       const updatedData = await User.findByIdAndUpdate({_id:user_id},{$set:{password:secure_password, token:''}});

       res.redirect("/");

        
    } catch (error) {

        console.log(error.message);
        
    }
}

//user profile load
const userProfileLoad = async(req, res)=>{
    try {
        const userData = await User.findById({ _id:req.session.user_id});
        res.render('approvalprofile',{user:userData});
        
    } catch (error) {
        console.log(error.message);
        
    }
}

//for verification send mail link
// const verificationLoad = async(req, res)=>{
//     try {

//         res.render('verification');
        
//     } catch (error) {
//         console.log(error.message);
        
//     }
// }

//verification
// const sentVerificationLink = async(req, res)=>{
//     try {

//         const email = req.body.email;
//         const userData = await User.findOne({email:email});
//         if(userData){
//             sendVerifyMail(userData.fname, userData.lname, userData.email,userData._id);
//             res.render('verification',{message:"Reset verification mail sent to your mail Id, please check."});
//         }
//         else{

//             res.render('verification',{message:"This email does not exist"});


//         }
        
//     } catch (error) {

//         console.log(error.message);
        
//     }
// }

//user profile edit and load
const editUserProfileLoad = async(req, res)=>{
    try {

        const id = req.query.id;
        const userData = await User.findById({_id:id});
        if(userData){
            res.render('editapprovalprofile',{user:userData});
        }
        else{
            res.redirect('/approvalprofile');
        }
        
    } catch (error) {

        console.log(error.message);
        
    }
}

//update user profile
const UpdateUserProfile = async(req,res)=>{
    try {

        if(req.file){
            const userData = await User.findByIdAndUpdate({_id:req.body.user_id},{$set:{name:req.body.name,userid:req.body.userid,email:req.body.email,mobilenumber:req.body.mno,image:req.file.filename}});
        }
        else{

            const userData = await User.findByIdAndUpdate({_id:req.body.user_id},{$set:{name:req.body.name,userid:req.body.userid,email:req.body.email,mobilenumber:req.body.mno}});
        }
        res.redirect('/approvalprofile')
        
    } catch (error) {

        console.log(error.message);
        
    }
}
const viewLoanRequests = async (req, res) => {
  try {
    const approvals = await Approval.find({ department: req.user.department })
      .populate("user")
      .populate("item");
    res.status(200).json({ approvals });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: "Server error" });
  }
};

const approveLoan = async (req, res) => {
  const { approvalId } = req.params;
  const { status } = req.body;
  try {
    const approval = await Approval.findById(approvalId).populate("user").populate("item");
    if (!approval) {
      return res.status(404).json({ message: "Approval not found" });
    }
    if (approval.status !== "pending") {
      return res.status(400).json({ message: "Approval already processed" });
    }
    if (status !== "approved" && status !== "rejected") {
      return res.status(400).json({ message: "Invalid status value" });
    }
    const loan = new Loan({
      user_id: approval.user._id,
      item: approval.item._id,
      quantity: 1, // Assuming that the quantity is always 1 for each loan request
      return_date: new Date(), // Assuming that the return date is always today for each loan request
      status,
    });
    await loan.save();
    approval.status = status;
    await approval.save();
    const userEmail = approval.user.email;
    const adminEmails = await User.find({ role: "admin" }, "email");
    const emailList = adminEmails.map((admin) => admin.email);
    const mailOptions = {
      from: "your_email@gmail.com",
      to: emailList,
      subject: `Loan Request ${status.toUpperCase()}`,
      text: `The loan request from ${approval.user.fullname} for ${approval.item.name} has been ${status}.`,
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
    const userMailOptions = {
      from: "your_email@gmail.com",
      to: userEmail,
      subject: `Loan Request ${status.toUpperCase()}`,
      text: `Your loan request for ${approval.item.name} has been ${status}.`,
    };
    transporter.sendMail(userMailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
    res.status(200).json({ message: "Loan request processed successfully" });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: "Server error" });
  }
};
module.exports={
  loadRegister,
  insertUser,
  verifyMail,
  loginLoad,
  verifyLogin,
  loadHome,
  userLogout,
  forgetLoad,
  forgetVerify,
  forgetPasswordLoad,
  resetPassword,
  // verificationLoad,
  // sentVerificationLink
  userProfileLoad,
  editUserProfileLoad,
  UpdateUserProfile,
  viewLoanRequests, 
  approveLoan
};