const express=require("express");
const user_route= express();
const session = require("express-session");

const config = require("../config/config");


approval_route.use(session({secret:config.sessionSecret,  resave: false, // set to false to avoid deprecated warning
saveUninitialized: false // set to false to avoid deprecated warning
}));



const auth = require("../middleware/auth");
const itemController = require('../controllers/itemController');

approval_route.set('view engine', 'ejs');
approval_route.set('views','./views/approval');

const bodyParser = require('body-parser');
approval_route.use(bodyParser.json());
approval_route.use(bodyParser.urlencoded({extended:true}))

const multer = require("multer");
const path = require("path");

approval_route.use(express.static('public'));

const storage = multer.diskStorage({
    destination:function(req,file,cb){
        cb(null,path.join(__dirname, '../public/userImages'));
    },
    filename:function(req,file,cb){
        const name =Date.now()+'-'+file.originalname;
        cb(null, name);
    }
});

const upload = multer({storage:storage});


const userController = require("../controllers/approvalController");

approval_route.get('/register',auth.isLogout,approvalController.loadRegister);

approval_route.post('/register',upload.single('image'),approvalController.insertUser);

approval_route.get('/verify', approvalController.verifyMail);

approval_route.get('/',auth.isLogout, approvalController.loginLoad);
approval_route.get('/login',auth.isLogout, approvalController.loginLoad);

approval_route.post('/login', approvalController.verifyLogin);

approval_route.get('/approvalhome',auth.isLogin,approvalController.loadHome);

approval_route.get('/logout', auth.isLogin,approvalController.userLogout);

approval_route.get('/forget', auth.isLogout, approvalController.forgetLoad);

approval_route.post('/forget', approvalController.forgetVerify);

approval_route.get('/forget-password',auth.isLogout, approvalController.forgetPasswordLoad);
approval_route.post('/forget-password', approvalController.resetPassword);

approval_route.get('/userprofile',auth.isLogin,approvalController.approvalProfileLoad);

approval_route.get('/edit',auth.isLogin,approvalController.editapprovalProfileLoad);
approval_route.post('/edit',upload.single('image'),approvalController.UpdateapprovalProfile);

approval_route.get('/viewitem', auth.isLogin, itemController.viewUserItems);

approval_route.get('/itemDetails/:id', auth.isLogin, itemController.viewItemDetails);




// user_route.get('/verification', userController.verificationLoad);
// user_route.post('/verification', userController.sentVerificationLink);

module.exports=user_route;
