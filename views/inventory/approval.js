const mongoose = require('mongoose');

const approvalSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  item: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Item",
    required: true,
  },
  department: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    enum: ["pending", "approved", "rejected"],
    default: "pending",
  },
});

approvalSchema.pre("save", async function (next) {
  try {
    const user = await User.findById(this.user);
    if (!user) {
      return next(new Error("User not found"));
    }
    const item = await Item.findById(this.item);
    if (!item) {
      return next(new Error("Item not found"));
    }
    if (item.available_items < this.quantity) {
      return next(new Error(`Only ${item.available_items} items are available for this loan`));
    }
    next();
  } catch (error) {
    next(error);
  }
});

const Approval = mongoose.model("Approval", approvalSchema);

module.exports = Approval;
