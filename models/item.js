const mongoose = require('mongoose');

const itemSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  category: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Category',
  },
  description: {
    type: String,
  },
  image: {
    type: String,
    required:true
  },
  available_items: {
    type: Number,
    default: 0,
  },
});

const Item = mongoose.model('Item', itemSchema);

module.exports = Item;
