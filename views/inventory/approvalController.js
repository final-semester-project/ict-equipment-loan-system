const Approval = require("./approval");
const Loan = require("./loan");
const User = require("../../models/userModel");
const item = require("../../models/item");
const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    user: "your_email@gmail.com",
    pass: "your_password",
  },
});

const viewLoanRequests = async (req, res) => {
  try {
    const approvals = await Approval.find({ department: req.user.department })
      .populate("user")
      .populate("item");
    res.status(200).json({ approvals });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: "Server error" });
  }
};

const approveLoan = async (req, res) => {
  const { approvalId } = req.params;
  const { status } = req.body;
  try {
    const approval = await Approval.findById(approvalId).populate("user").populate("item");
    if (!approval) {
      return res.status(404).json({ message: "Approval not found" });
    }
    if (approval.status !== "pending") {
      return res.status(400).json({ message: "Approval already processed" });
    }
    if (status !== "approved" && status !== "rejected") {
      return res.status(400).json({ message: "Invalid status value" });
    }
    const loan = new Loan({
      user_id: approval.user._id,
      item: approval.item._id,
      quantity: 1, // Assuming that the quantity is always 1 for each loan request
      return_date: new Date(), // Assuming that the return date is always today for each loan request
      status,
    });
    await loan.save();
    approval.status = status;
    await approval.save();
    const userEmail = approval.user.email;
    const adminEmails = await User.find({ role: "admin" }, "email");
    const emailList = adminEmails.map((admin) => admin.email);
    const mailOptions = {
      from: "your_email@gmail.com",
      to: emailList,
      subject: `Loan Request ${status.toUpperCase()}`,
      text: `The loan request from ${approval.user.fullname} for ${approval.item.name} has been ${status}.`,
    };
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
    const userMailOptions = {
      from: "your_email@gmail.com",
      to: userEmail,
      subject: `Loan Request ${status.toUpperCase()}`,
      text: `Your loan request for ${approval.item.name} has been ${status}.`,
    };
    transporter.sendMail(userMailOptions, function (error, info) {
      if (error) {
        console.log(error);
      } else {
        console.log("Email sent: " + info.response);
      }
    });
    res.status(200).json({ message: "Loan request processed successfully" });
  } catch (error) {
    console.log(error.message);
    res.status(500).json({ message: "Server error" });
  }
};

module.exports = { viewLoanRequests, approveLoan };
