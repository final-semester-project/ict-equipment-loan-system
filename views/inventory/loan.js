const mongoose = require('mongoose');

const loanSchema = new mongoose.Schema({
  loan_id: {
    type: Number,
    required: true,
  },
  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  item: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Item",
    required: true,
  },
  quantity: {
    type: Number,
    required: true,
  },
  return_date: {
    type: Date,
    required: true,
  },
  status: {
    type: String,
    enum: ["pending", "approved", "rejected"],
    default: "pending",
  },
  admin_collection_date: {
    type: Date,
  },
});

loanSchema.pre("save", async function (next) {
  try {
    const user = await User.findById(this.user_id);
    if (!user) {
      return next(new Error("User not found"));
    }
    if (user.department.toString() !== this.department.toString()) {
      return next(
        new Error(
          "User and approval should be from the same department"
        )
      );
    }
    const item = await Item.findById(this.item);
    if (!item) {
      return next(new Error("Item not found"));
    }
    if (item.available_items < this.quantity) {
      return next(new Error(`Only ${item.available_items} items are available for this loan`));
    }
    item.available_items -= this.quantity;
    await item.save();
    next();
  } catch (error) {
    next(error);
  }
});
