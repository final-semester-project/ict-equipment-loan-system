<mxfile host="app.diagrams.net" modified="2023-06-11T17:15:47.854Z" agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36" etag="xhEwoBAggjf7tw8t6zns" version="21.3.8" type="gitlab">
  <diagram id="C5RBs43oDa-KdzZeNtuy" name="Page-1">
    <mxGraphModel dx="3060" dy="1500" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="980" pageHeight="1390" math="0" shadow="0">
      <root>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-0" />
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-1" parent="WIyWlLk6GJQsqaUBKTNV-0" />
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-73" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="WIyWlLk6GJQsqaUBKTNV-1" edge="1">
          <mxGeometry y="20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="740" y="420" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-77" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="WIyWlLk6GJQsqaUBKTNV-1" edge="1">
          <mxGeometry y="20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="740" y="550" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-84" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="WIyWlLk6GJQsqaUBKTNV-1" edge="1">
          <mxGeometry y="20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="740" y="780" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-88" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="WIyWlLk6GJQsqaUBKTNV-1" edge="1">
          <mxGeometry y="20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="740" y="880" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="kzVZhLHagi3wZbfEgIwt-6" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="WIyWlLk6GJQsqaUBKTNV-1" edge="1">
          <mxGeometry y="20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="920" y="1110" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="iMZaKyPBan3ShcJHyNA--4" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="WIyWlLk6GJQsqaUBKTNV-1" edge="1">
          <mxGeometry y="20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="740" y="490" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="uk9T2SMFnwhvLfq1B6Z4-20" value="" style="group" parent="WIyWlLk6GJQsqaUBKTNV-1" vertex="1" connectable="0">
          <mxGeometry x="290" y="40" width="1380" height="1340" as="geometry" />
        </mxCell>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-6" value="Authentication" style="rhombus;whiteSpace=wrap;html=1;shadow=0;fontFamily=Helvetica;fontSize=12;align=center;strokeWidth=1;spacing=6;spacingTop=-4;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="620" y="180" width="100" height="80" as="geometry" />
        </mxCell>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-7" value="User Dashboard" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="1060" y="100" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-5" value="Is User" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;entryX=0;entryY=0.5;entryDx=0;entryDy=0;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="WIyWlLk6GJQsqaUBKTNV-6" target="WIyWlLk6GJQsqaUBKTNV-7" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <Array as="points">
              <mxPoint x="730" y="220" />
              <mxPoint x="730" y="160" />
              <mxPoint x="1030" y="160" />
              <mxPoint x="1030" y="120" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-0" value="Start" style="ellipse;whiteSpace=wrap;html=1;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="610" width="120" height="80" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-7" value="No" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-6" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="1090" y="440" as="sourcePoint" />
            <mxPoint x="1180" y="250" as="targetPoint" />
            <Array as="points">
              <mxPoint x="1240" y="420" />
              <mxPoint x="1240" y="250" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-28" value="Found Equipment" style="rhombus;whiteSpace=wrap;html=1;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="920" y="280" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-29" value="No" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=0;exitY=0.5;exitDx=0;exitDy=0;entryX=0.153;entryY=0.99;entryDx=0;entryDy=0;entryPerimeter=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-28" target="WIyWlLk6GJQsqaUBKTNV-7" edge="1">
          <mxGeometry x="-0.2196" y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="730" y="190" as="sourcePoint" />
            <mxPoint x="860" y="220" as="targetPoint" />
            <Array as="points">
              <mxPoint x="910" y="320" />
              <mxPoint x="910" y="170" />
              <mxPoint x="1078" y="170" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-30" value="Yes" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;entryX=0;entryY=0.5;entryDx=0;entryDy=0;exitX=1;exitY=0.5;exitDx=0;exitDy=0;entryPerimeter=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-28" target="fHExBn2Ex2H_X2-pyjgQ-18" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="1010" y="270" as="sourcePoint" />
            <mxPoint x="990" y="190" as="targetPoint" />
            <Array as="points">
              <mxPoint x="1000" y="320" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-33" value="Approval Dashboard" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="920" y="455" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-92" value="End" style="ellipse;whiteSpace=wrap;html=1;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="1260" width="120" height="80" as="geometry" />
        </mxCell>
        <mxCell id="fR9akq4NeD1LBSidLvgX-0" value="Login" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="610" y="110" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fR9akq4NeD1LBSidLvgX-1" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="WIyWlLk6GJQsqaUBKTNV-6" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="670" y="150" as="sourcePoint" />
            <mxPoint x="670" y="190" as="targetPoint" />
            <Array as="points">
              <mxPoint x="670" y="162" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fR9akq4NeD1LBSidLvgX-2" value="No" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=0;exitY=0.5;exitDx=0;exitDy=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="WIyWlLk6GJQsqaUBKTNV-6" target="fR9akq4NeD1LBSidLvgX-0" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="850" y="280" as="sourcePoint" />
            <mxPoint x="610" y="140" as="targetPoint" />
            <Array as="points">
              <mxPoint x="580" y="220" />
              <mxPoint x="580" y="130" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-2" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;exitX=0.5;exitY=1;exitDx=0;exitDy=0;entryX=0.417;entryY=0;entryDx=0;entryDy=0;entryPerimeter=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-0" target="fR9akq4NeD1LBSidLvgX-0" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="670" y="90" as="sourcePoint" />
            <mxPoint x="670" y="120" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="iMZaKyPBan3ShcJHyNA--2" value="Is Approval" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=1;exitY=0.5;exitDx=0;exitDy=0;edgeStyle=orthogonalEdgeStyle;entryX=0;entryY=0.5;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="WIyWlLk6GJQsqaUBKTNV-6" target="fHExBn2Ex2H_X2-pyjgQ-33" edge="1">
          <mxGeometry x="0.011" y="30" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="1150" y="480" as="sourcePoint" />
            <mxPoint x="920" y="472" as="targetPoint" />
            <Array as="points">
              <mxPoint x="790" y="220" />
              <mxPoint x="790" y="475" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-9" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=0.5;exitY=1;exitDx=0;exitDy=0;entryX=1;entryY=0.5;entryDx=0;entryDy=0;edgeStyle=orthogonalEdgeStyle;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-6" target="fHExBn2Ex2H_X2-pyjgQ-33" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="1200" y="419.5" as="sourcePoint" />
            <mxPoint x="1140" y="530" as="targetPoint" />
            <Array as="points">
              <mxPoint x="1140" y="475" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-80" value="Collect Within Duration" style="rhombus;whiteSpace=wrap;html=1;shadow=0;fontFamily=Helvetica;fontSize=12;align=center;strokeWidth=1;spacing=6;spacingTop=-4;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="640" y="620" width="100" height="80" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-82" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;startArrow=none;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-80" edge="1">
          <mxGeometry x="0.2" y="-20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="669.9300000000001" y="570" as="sourcePoint" />
            <mxPoint x="690" y="730" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="PFrYGDOzKWU2OFUAZQn8-2" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="1128" y="140" as="sourcePoint" />
            <mxPoint x="1120" y="220" as="targetPoint" />
            <Array as="points">
              <mxPoint x="1120" y="140" />
              <mxPoint x="1120" y="210" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="kzVZhLHagi3wZbfEgIwt-3" value="Return With Damage" style="rhombus;whiteSpace=wrap;html=1;shadow=0;fontFamily=Helvetica;fontSize=12;align=center;strokeWidth=1;spacing=6;spacingTop=-4;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="1040" width="120" height="80" as="geometry" />
        </mxCell>
        <mxCell id="kzVZhLHagi3wZbfEgIwt-8" value="No" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;startArrow=none;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry x="-0.5558" y="-10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="689.74" y="1160" as="sourcePoint" />
            <mxPoint x="689.74" y="1170" as="targetPoint" />
            <Array as="points">
              <mxPoint x="689.74" y="1120" />
              <mxPoint x="689.74" y="1120" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="I4XyzJXzLZ_4KcgwyHx7-1" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="I4XyzJXzLZ_4KcgwyHx7-0" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="980" y="494" as="sourcePoint" />
            <mxPoint x="974" y="530" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="kzVZhLHagi3wZbfEgIwt-15" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="689.6800000000001" y="1190" as="sourcePoint" />
            <mxPoint x="689.6800000000001" y="1260" as="targetPoint" />
            <Array as="points">
              <mxPoint x="689.6800000000001" y="1240" />
              <mxPoint x="689.6800000000001" y="1240" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-11" value="View All Equipments" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="1060" y="220" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-24" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;exitX=0.25;exitY=1;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="p1lYlfQaSRWjq5VlP56w-18" target="fHExBn2Ex2H_X2-pyjgQ-28" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="980" y="270" as="sourcePoint" />
            <mxPoint x="910" y="260" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-86" value="No" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=1;exitY=0.5;exitDx=0;exitDy=0;entryX=1;entryY=0.25;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-80" target="fHExBn2Ex2H_X2-pyjgQ-11" edge="1">
          <mxGeometry x="-0.0642" y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="800" y="760" as="sourcePoint" />
            <mxPoint x="1090" y="240" as="targetPoint" />
            <Array as="points">
              <mxPoint x="740" y="690" />
              <mxPoint x="1260" y="690" />
              <mxPoint x="1260" y="230" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-17" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="1140" y="260" as="sourcePoint" />
            <mxPoint x="1140" y="300" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-18" value="View Equipment Details" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="1080" y="300" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-6" value="Apply Loan Request" style="rhombus;whiteSpace=wrap;html=1;shadow=0;fontFamily=Helvetica;fontSize=12;align=center;strokeWidth=1;spacing=6;spacingTop=-4;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="1090" y="380" width="100" height="80" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-21" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0.5;entryY=0;entryDx=0;entryDy=0;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-18" target="fHExBn2Ex2H_X2-pyjgQ-6" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="1150" y="340" as="sourcePoint" />
            <mxPoint x="1130" y="390" as="targetPoint" />
            <Array as="points">
              <mxPoint x="1140" y="340" />
              <mxPoint x="1140" y="340" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-45" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="fHExBn2Ex2H_X2-pyjgQ-43" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="980" y="570" as="sourcePoint" />
            <mxPoint x="1000" y="560" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="I4XyzJXzLZ_4KcgwyHx7-0" value="View Pending Loan Request" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="920" y="530" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-43" value="Loan Approved" style="rhombus;whiteSpace=wrap;html=1;shadow=0;fontFamily=Helvetica;fontSize=12;align=center;strokeWidth=1;spacing=6;spacingTop=-4;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="930" y="600" width="100" height="80" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-52" value="No" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;entryX=1;entryY=0.5;entryDx=0;entryDy=0;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-43" target="fHExBn2Ex2H_X2-pyjgQ-11" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="1030" y="610" as="sourcePoint" />
            <mxPoint x="1100" y="240" as="targetPoint" />
            <Array as="points">
              <mxPoint x="1250" y="640" />
              <mxPoint x="1250" y="240" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-4" value="Is Admin" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0.411;entryY=-0.025;entryDx=0;entryDy=0;entryPerimeter=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="WIyWlLk6GJQsqaUBKTNV-6" target="WIyWlLk6GJQsqaUBKTNV-12" edge="1">
          <mxGeometry x="0.0484" y="-20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="660" y="300" as="targetPoint" />
            <Array as="points">
              <mxPoint x="670" y="280" />
              <mxPoint x="540" y="280" />
              <mxPoint x="540" y="300" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="WIyWlLk6GJQsqaUBKTNV-12" value="Admin Dashboard" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="490" y="300" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-74" value="Update Equipments" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="300" y="720" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-78" value="Add Category" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="560" y="400" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-85" value="On Loan" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="870" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-89" value="Equipment Return" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="940" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="kzVZhLHagi3wZbfEgIwt-7" value="Equipment Returned" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="1170" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="4lUKnLMnXLiY4Hf8ofPM-0" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="mOcLxqL09k10rBN-BkU5-26" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="550" y="370" as="sourcePoint" />
            <mxPoint x="690" y="420" as="targetPoint" />
            <Array as="points">
              <mxPoint x="690" y="370" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="rBjotmAtzKScSrV_FtwN-3" value="Remove Equipments" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="20" y="720" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="rBjotmAtzKScSrV_FtwN-5" value="Add Users" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="430" y="400" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="rBjotmAtzKScSrV_FtwN-6" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="490" y="370" as="sourcePoint" />
            <mxPoint x="490" y="400" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="rBjotmAtzKScSrV_FtwN-9" value="Remove Users" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="450" y="600" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-2" value="View all User" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="300" y="400" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-3" value="View By User" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="300" y="470" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-5" value="View Equipments" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="160" y="400" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-6" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="359.64" y="370" as="sourcePoint" />
            <mxPoint x="359.64" y="400" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-7" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="359.64" y="440" as="sourcePoint" />
            <mxPoint x="359.64" y="470" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-8" value="View Equipment Detail" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="160" y="550" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-11" value="" style="group" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1" connectable="0">
          <mxGeometry x="20" y="450" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-10" value="Yes" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="mOcLxqL09k10rBN-BkU5-11" source="mOcLxqL09k10rBN-BkU5-9" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="100" y="39.889999999999986" as="sourcePoint" />
            <mxPoint x="140" y="120" as="targetPoint" />
            <Array as="points">
              <mxPoint x="110" y="130" />
              <mxPoint x="110" y="120" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-12" value="No" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="mOcLxqL09k10rBN-BkU5-9" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="-140" y="450" as="sourcePoint" />
            <mxPoint x="490" y="330" as="targetPoint" />
            <Array as="points">
              <mxPoint x="-190" y="580" />
              <mxPoint x="-190" y="330" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-13" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="219.68999999999994" y="590" as="sourcePoint" />
            <mxPoint x="219.68999999999994" y="620" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-14" value="Manage Equipments" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="160" y="620" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-19" value="" style="group" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1" connectable="0">
          <mxGeometry x="320" y="540" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-20" value="Active User" style="rhombus;whiteSpace=wrap;html=1;container=0;" parent="mOcLxqL09k10rBN-BkU5-19" vertex="1">
          <mxGeometry width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-21" value="Yes" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=0;exitY=0.5;exitDx=0;exitDy=0;entryX=0.056;entryY=0.976;entryDx=0;entryDy=0;entryPerimeter=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="mOcLxqL09k10rBN-BkU5-20" target="mOcLxqL09k10rBN-BkU5-2" edge="1">
          <mxGeometry x="-0.165" y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="420" y="559.89" as="sourcePoint" />
            <mxPoint x="310" y="450" as="targetPoint" />
            <Array as="points">
              <mxPoint x="290" y="580" />
              <mxPoint x="290" y="460" />
              <mxPoint x="307" y="460" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-22" value="No" style="edgeStyle=orthogonalEdgeStyle;rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry x="-0.2903" y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="400" y="580" as="sourcePoint" />
            <mxPoint x="510" y="600" as="targetPoint" />
            <Array as="points">
              <mxPoint x="400" y="580" />
              <mxPoint x="510" y="580" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-24" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;entryX=0.5;entryY=0;entryDx=0;entryDy=0;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="mOcLxqL09k10rBN-BkU5-5" target="mOcLxqL09k10rBN-BkU5-8" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="219.68999999999994" y="510" as="sourcePoint" />
            <mxPoint x="219.68999999999994" y="540" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-25" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="619.63" y="370" as="sourcePoint" />
            <mxPoint x="619.63" y="400" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-26" value="Pending For Collection" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="470" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-27" value="Ready For Collection" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="540" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-28" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="689.62" y="510" as="sourcePoint" />
            <mxPoint x="690" y="540" as="targetPoint" />
            <Array as="points">
              <mxPoint x="690" y="540" />
              <mxPoint x="690" y="540" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-29" value="Accept Equipment Collection" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="730" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-31" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="689.68" y="580" as="sourcePoint" />
            <mxPoint x="689.68" y="620" as="targetPoint" />
            <Array as="points">
              <mxPoint x="689.68" y="610" />
              <mxPoint x="689.68" y="610" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-32" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;startArrow=none;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry x="0.2" y="-20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="689.68" y="910" as="sourcePoint" />
            <mxPoint x="689.68" y="940" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-35" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;startArrow=none;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry x="0.2" y="-20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="689.68" y="770" as="sourcePoint" />
            <mxPoint x="689.68" y="800" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-36" value="Collected" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="630" y="800" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-37" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;startArrow=none;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry x="0.2" y="-20" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="689.68" y="840" as="sourcePoint" />
            <mxPoint x="689.68" y="870" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-38" value="" style="endArrow=none;html=1;rounded=0;entryX=0.5;entryY=0;entryDx=0;entryDy=0;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-89" target="kzVZhLHagi3wZbfEgIwt-3" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="750" y="980" as="sourcePoint" />
            <mxPoint x="800" y="930" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="uk9T2SMFnwhvLfq1B6Z4-0" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;exitX=1;exitY=0.5;exitDx=0;exitDy=0;entryX=0.75;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="kzVZhLHagi3wZbfEgIwt-7" target="fHExBn2Ex2H_X2-pyjgQ-11" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="460" y="1180" as="sourcePoint" />
            <mxPoint x="1150" y="220" as="targetPoint" />
            <Array as="points">
              <mxPoint x="1380" y="1190" />
              <mxPoint x="1380" y="170" />
              <mxPoint x="1150" y="170" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="uk9T2SMFnwhvLfq1B6Z4-4" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0;entryY=0.5;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="WIyWlLk6GJQsqaUBKTNV-12" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="630" y="1080" as="sourcePoint" />
            <mxPoint x="478.3838630445076" y="310" as="targetPoint" />
            <Array as="points">
              <mxPoint x="-230" y="1080" />
              <mxPoint x="-230" y="320" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="wD9W-TzsIgE4sm31tWAA-4" value="Yes" style="edgeLabel;html=1;align=center;verticalAlign=middle;resizable=0;points=[];" vertex="1" connectable="0" parent="uk9T2SMFnwhvLfq1B6Z4-4">
          <mxGeometry x="-0.751" y="1" relative="1" as="geometry">
            <mxPoint as="offset" />
          </mxGeometry>
        </mxCell>
        <mxCell id="uk9T2SMFnwhvLfq1B6Z4-10" value="" style="group" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1" connectable="0">
          <mxGeometry x="80" y="690" width="280" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-16" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-10" target="fHExBn2Ex2H_X2-pyjgQ-74" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="140" as="sourcePoint" />
            <mxPoint x="280" y="-60" as="targetPoint" />
            <Array as="points">
              <mxPoint x="140" />
              <mxPoint x="280" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-15" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="mOcLxqL09k10rBN-BkU5-14" target="rBjotmAtzKScSrV_FtwN-3" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="459.95" y="635" as="sourcePoint" />
            <mxPoint x="129.95" y="695" as="targetPoint" />
            <Array as="points">
              <mxPoint x="220" y="690" />
              <mxPoint x="80" y="690" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-44" value="Yes" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;exitX=0;exitY=0.5;exitDx=0;exitDy=0;edgeStyle=orthogonalEdgeStyle;entryX=1;entryY=0.5;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="fHExBn2Ex2H_X2-pyjgQ-43" target="mOcLxqL09k10rBN-BkU5-26" edge="1">
          <mxGeometry y="10" relative="1" as="geometry">
            <mxPoint as="offset" />
            <mxPoint x="1100" y="679.5" as="sourcePoint" />
            <mxPoint x="760" y="490" as="targetPoint" />
            <Array as="points">
              <mxPoint x="860" y="640" />
              <mxPoint x="860" y="490" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="p1lYlfQaSRWjq5VlP56w-1" value="Search Equipments" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="-30" y="400" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mOcLxqL09k10rBN-BkU5-9" value="Found Equipment" style="rhombus;whiteSpace=wrap;html=1;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="-20" y="540" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="p1lYlfQaSRWjq5VlP56w-9" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="mOcLxqL09k10rBN-BkU5-5" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="220" y="370" as="sourcePoint" />
            <mxPoint x="369.64" y="410" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="p1lYlfQaSRWjq5VlP56w-10" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="mOcLxqL09k10rBN-BkU5-9" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="19.229999999999933" y="440" as="sourcePoint" />
            <mxPoint x="19.539999999999992" y="480" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="p1lYlfQaSRWjq5VlP56w-14" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" target="p1lYlfQaSRWjq5VlP56w-1" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="30" y="370" as="sourcePoint" />
            <mxPoint x="369.64" y="410" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="p1lYlfQaSRWjq5VlP56w-17" value="" style="endArrow=classic;html=1;rounded=0;endFill=0;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" source="mOcLxqL09k10rBN-BkU5-3" edge="1">
          <mxGeometry width="50" height="50" relative="1" as="geometry">
            <mxPoint x="369.64" y="450" as="sourcePoint" />
            <mxPoint x="360" y="540" as="targetPoint" />
          </mxGeometry>
        </mxCell>
        <mxCell id="p1lYlfQaSRWjq5VlP56w-18" value="Search Equipment&lt;br&gt;" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="uk9T2SMFnwhvLfq1B6Z4-20" vertex="1">
          <mxGeometry x="930" y="200" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="p1lYlfQaSRWjq5VlP56w-20" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;" parent="uk9T2SMFnwhvLfq1B6Z4-20" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="1120" y="180" as="sourcePoint" />
            <mxPoint x="980" y="200" as="targetPoint" />
            <Array as="points">
              <mxPoint x="980" y="180" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="rBjotmAtzKScSrV_FtwN-1" value="Add Equipments" style="rounded=1;whiteSpace=wrap;html=1;fontSize=12;glass=0;strokeWidth=1;shadow=0;container=0;" parent="WIyWlLk6GJQsqaUBKTNV-1" vertex="1">
          <mxGeometry x="110" y="440" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="fHExBn2Ex2H_X2-pyjgQ-75" value="" style="rounded=0;html=1;jettySize=auto;orthogonalLoop=1;fontSize=11;endArrow=block;endFill=0;endSize=8;strokeWidth=1;shadow=0;labelBackgroundColor=none;edgeStyle=orthogonalEdgeStyle;exitX=0.5;exitY=1;exitDx=0;exitDy=0;entryX=0.417;entryY=-0.066;entryDx=0;entryDy=0;entryPerimeter=0;" parent="WIyWlLk6GJQsqaUBKTNV-1" source="WIyWlLk6GJQsqaUBKTNV-12" target="rBjotmAtzKScSrV_FtwN-1" edge="1">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="960" y="390" as="sourcePoint" />
            <mxPoint x="310" y="440" as="targetPoint" />
            <Array as="points">
              <mxPoint x="840" y="410" />
              <mxPoint x="160" y="410" />
            </Array>
          </mxGeometry>
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
